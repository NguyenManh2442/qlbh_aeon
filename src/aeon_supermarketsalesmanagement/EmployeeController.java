/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aeon_supermarketsalesmanagement;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author TIEN MANH
 */
public class EmployeeController {

    public static ArrayList<Employee> getAllEmployee() {
        ArrayList<Employee> list = new ArrayList<>();
        Connection conn = DBConnection.getConnection();
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM NHANVIEN");
            while (rs.next()) {
                int id = rs.getInt("id");
                String MaSv = rs.getString("MaNV");
                String HoTen = rs.getString("TenNV");
                int Tuoi = rs.getInt("Tuoi");
                String DiaChi = rs.getString("DiaChi");
                int SoCMND = rs.getInt("SoCMND");

                Employee emp = new Employee(id, MaSv, HoTen, Tuoi, DiaChi, SoCMND);
                list.add(emp);
            }
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public static void addEmployee(Employee epl) {
        Connection conn = DBConnection.getConnection();
        String sql = "INSERT INTO NHANVIEN(MaNV, TenNV, Tuoi, DiaChi, SoCMND) VALUES(?, ?, ?, ?, ?)";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, epl.getMaNV());
            ps.setString(2, epl.getTen());
            ps.setInt(3, epl.getTuoi());
            ps.setString(4, epl.getDiaChi());
            ps.setInt(5, epl.getSoCMND());
            ps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void editEmployee(Employee epl) {
        Connection conn = DBConnection.getConnection();
        String sql = "UPDATE NHANVIEN SET MaNV = ?, TenNV = ?, Tuoi=?, DiaChi=?, SoCMND=? WHERE id = ?";
        try {

            PreparedStatement ps = conn.prepareStatement(sql);

            ps.setString(1, epl.getMaNV());
            ps.setString(2, epl.getTen());
            ps.setInt(3, epl.getTuoi());
            ps.setString(4, epl.getDiaChi());
            ps.setInt(5, epl.getSoCMND());
            ps.setInt(6, epl.getid());
            ps.execute();

        } catch (SQLException ex) {
            Logger.getLogger(EmployeeController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void deleteEmployee(int id) {
        Connection conn = DBConnection.getConnection();
        String sql = "DELETE from NHANVIEN where id = ?";
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);

            ps.execute();

        } catch (SQLException ex) {
            Logger.getLogger(EmployeeController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static ArrayList<Employee> seachEmployee(String tennv) {
        ArrayList<Employee> list = new ArrayList<>();
        Connection conn = DBConnection.getConnection();
        try {
            Statement st = conn.createStatement();
            String tennv2 = "%" + tennv + "%";
            String sql = "SELECT * FROM NHANVIEN WHERE TenNV LIKE '" + tennv2 + "'";
            ResultSet rs = st.executeQuery(sql);
            PreparedStatement ps;
            while (rs.next()) {
                int id = rs.getInt("id");
                String MaSv = rs.getString("MaNV");
                String HoTen = rs.getString("TenNV");
                int Tuoi = rs.getInt("Tuoi");
                String DiaChi = rs.getString("DiaChi");
                int SoCMND = rs.getInt("SoCMND");

                Employee emp = new Employee(id, MaSv, HoTen, Tuoi, DiaChi, SoCMND);
                list.add(emp);
            }
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;

    }

}
