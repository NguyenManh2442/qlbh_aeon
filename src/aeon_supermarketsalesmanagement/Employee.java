/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aeon_supermarketsalesmanagement;

import java.util.Vector;

/**
 *
 * @author TIEN MANH
 */
public class Employee {

    private int id;
    private String MaNV;
    private String Ten;
    private int Tuoi;
    private String DiaChi;
    private int SoCMND;

    public Employee() {

    }

    public Employee(String MaNV, String Ten, int Tuoi, String DiaChi, int SoCMND) {
        this.MaNV = MaNV;
        this.Ten = Ten;
        this.Tuoi = Tuoi;
        this.DiaChi = DiaChi;
        this.SoCMND = SoCMND;
    }

    public Employee(int id, String MaNV, String Ten, int Tuoi, String DiaChi, int SoCMND) {
        this.id = id;
        this.MaNV = MaNV;
        this.Ten = Ten;
        this.Tuoi = Tuoi;
        this.DiaChi = DiaChi;
        this.SoCMND = SoCMND;
    }


    public int getid() {
        return id;
    }

    public String getMaNV() {
        return MaNV;
    }

    public String getTen() {
        return Ten;
    }

    public int getTuoi() {
        return Tuoi;
    }

    public String getDiaChi() {
        return DiaChi;
    }

    public int getSoCMND() {
        return SoCMND;
    }

    public void setid(int id) {
        this.id = id;
    }

    public void setMaNV(String MaNV) {
        this.MaNV = MaNV;
    }

    public void setTen(String Ten) {
        this.Ten = Ten;
    }

    public void setTuoi(int Tuoi) {
        this.Tuoi = Tuoi;
    }

    public void setDiaChi(String DiaChi) {
        this.DiaChi = DiaChi;
    }

    public void setSoCMND(int SoCMND) {
        this.SoCMND = SoCMND;
    }

    public Object[] toArray() {
        return new Object[]{id, MaNV, Ten, Tuoi, DiaChi, SoCMND};
    }

}
