/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aeon_supermarketsalesmanagement;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tienmanh
 */
public class WarehouseController {

    public static ArrayList<Warehouse> getAllProduct() {
        ArrayList<Warehouse> list = new ArrayList<>();
        Connection conn = DBConnection.getConnection();
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT c.id, c.SoPn ,HANG.TenHang , c.MaHang , c.SlNhap , c.DgNhap from CTPNHAP c  JOIN HANG  on HANG.MaHang  = c.MaHang ;");
            while (rs.next()) {
                int id = rs.getInt("id");
                String SoPn = rs.getString("SoPn");
                String MaHang = rs.getString("MaHang");
                String TenHang = rs.getString("TenHang");
                int SL = rs.getInt("SlNhap");
                int Gia = rs.getInt("DgNhap");

                Warehouse emp = new Warehouse(id, SoPn, MaHang, TenHang, SL, Gia);
                list.add(emp);
            }
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public static ArrayList<Warehouse> getMaHang() {
        ArrayList<Warehouse> list = new ArrayList<>();
        Connection conn = DBConnection.getConnection();
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT  MaHang FROM  HANG ;");
            while (rs.next()) {

                String MaHang = rs.getString("MaHang");

                Warehouse emp = new Warehouse(MaHang);
                list.add(emp);
            }
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public static ArrayList<Warehouse> getSoHoaDon() {
        ArrayList<Warehouse> list = new ArrayList<>();
        Connection conn = DBConnection.getConnection();
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT  SoPn FROM  PNHAP;");
            while (rs.next()) {

                String SoPn = rs.getString("SoPn");

                Warehouse emp = new Warehouse(SoPn, 2);
                list.add(emp);
            }
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public static void addProduct(Warehouse epl) {
        Connection conn = DBConnection.getConnection();
        String sql = "INSERT INTO CTPNHAP(SoPn, MaHang, SlNhap, DgNhap) VALUES(?, ?, ?, ?)";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, epl.getSoPn());
            ps.setString(2, epl.getMaHang());
            ps.setInt(3, epl.getSoLuong());
            ps.setInt(4, epl.getDongia());
            ps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void editProduct(Warehouse epl) {
        Connection conn = DBConnection.getConnection();
        String sql = "UPDATE CTPNHAP SET SoPn = ?, MaHang = ?, SlNhap=?, DgNhap=? WHERE id = ?";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, epl.getSoPn());
            ps.setString(2, epl.getMaHang());
            ps.setInt(3, epl.getSoLuong());
            ps.setInt(4, epl.getDongia());
            ps.setInt(5, epl.getId());
            ps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void deleteProduct(int id) {
        Connection conn = DBConnection.getConnection();
        String sql = "DELETE FROM CTPNHAP  WHERE id = ?";
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);

            ps.execute();

        } catch (SQLException ex) {
            Logger.getLogger(EmployeeController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static ArrayList<Warehouse> seachProduct(String tenhang) {
        ArrayList<Warehouse> list = new ArrayList<>();
        Connection conn = DBConnection.getConnection();
        try {
            Statement st = conn.createStatement();
            String tenhang2 = "%" + tenhang + "%";
            String sql = "SELECT c.id, c.SoPn ,HANG.TenHang , c.MaHang , c.SlNhap , c.DgNhap from CTPNHAP c  JOIN HANG  on HANG.MaHang  = c.MaHang WHERE HANG.TenHang LIKE '" + tenhang2 + "'";
            ResultSet rs = st.executeQuery(sql);
            PreparedStatement ps;
            while (rs.next()) {
                int id = rs.getInt("id");
                String SoPn = rs.getString("SoPn");
                String MaHang = rs.getString("MaHang");
                String TenHang = rs.getString("TenHang");
                int SL = rs.getInt("SlNhap");
                int Gia = rs.getInt("DgNhap");

                Warehouse emp = new Warehouse(id, SoPn, MaHang, TenHang, SL, Gia);
                list.add(emp);
            }
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;

    }
}
