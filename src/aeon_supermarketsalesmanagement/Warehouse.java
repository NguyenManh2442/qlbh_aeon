/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aeon_supermarketsalesmanagement;

import java.util.Vector;

/**
 *
 * @author tienmanh
 */
public class Warehouse {

   
    private int id;
    private String SoPn;
    private String MaHang;
    private String TenHang;
    private int SoLuong;
    private int Dongia;

    public Warehouse(int id, String SoPn, String MaHang, String TenHang, int SoLuong, int Dongia) {
        this.id = id;
        this.SoPn = SoPn;
        this.MaHang = MaHang;
        this.TenHang = TenHang;
        this.SoLuong = SoLuong;
        this.Dongia = Dongia;
    }
    public Warehouse(String SoPn, String MaHang, int SoLuong, int Dongia) {
        this.SoPn = SoPn;
        this.MaHang = MaHang;
        this.SoLuong = SoLuong;
        this.Dongia = Dongia;
    }
    public Warehouse(int id, String SoPn, String MaHang, int SoLuong, int Dongia) {
        this.id = id;
        this.SoPn = SoPn;
        this.MaHang = MaHang;
        this.SoLuong = SoLuong;
        this.Dongia = Dongia;
    }
    Warehouse(String MaHang) {
       this.MaHang = MaHang;
    }
    Warehouse(String SoPn, int a) {
       this.SoPn = SoPn;
    }
    public int getId() {
        return id;
    }

    public String getSoPn() {
        return SoPn;
    }

    public String getMaHang() {
        return MaHang;
    }

    public String getTenHang() {
        return TenHang;
    }

    public int getSoLuong() {
        return SoLuong;
    }

    public int getDongia() {
        return Dongia;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setSoPn(String SoPn) {
        this.SoPn = SoPn;
    }

    public void setMaHang(String MaHang) {
        this.MaHang = MaHang;
    }

    public void setTenHang(String TenHang) {
        this.TenHang = TenHang;
    }

    public void setSoLuong(int SoLuong) {
        this.SoLuong = SoLuong;
    }

    public void setDongia(int Dongia) {
        this.Dongia = Dongia;
    }
    
    public Object[] toArray() {
        return new Object[]{id, SoPn, MaHang, TenHang, SoLuong, Dongia};
    }
    public Object[] toArrayMaHd() {
        return new Object[]{ MaHang};
    }
    public Object[] toArraySoPn() {
        return new Object[]{ SoPn };
    }
}
